﻿using System;

namespace WebSocketServer
{
    class Program
    {
        static void Main(string[] args)
        {
            WebSocketServerTest WSServerTest = new WebSocketServerTest();
            WSServerTest.Start();
        }
    }
}
